//
//  iOSMacros.h
//  Matchismo
//
//  Created by Arjan on 04/08/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//

#ifndef Macros_iOSMacros_h
#define Macros_iOSMacros_h

#define FORMAT(string, args...) [NSString stringWithFormat:string, args]

#define ALERT(title, msg) [[[UIAlertView alloc]     initWithTitle:title\
message:msg\
delegate:nil\
cancelButtonTitle:@"OK"\
otherButtonTitles:nil] show]

#define URLIFY(urlString) [NSURL URLWithString:urlString] 

#endif