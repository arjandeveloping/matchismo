//
//  SetCard.m
//  Matchismo
//
//  Created by Arjan on 08/08/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//

#import "SetCardView.h"
@interface SetCardView ()
@property (nonatomic) CGFloat setCardSymbolGroupScaleFactor;
@end

@implementation SetCardView
@synthesize setCardSymbolGroupScaleFactor = _setCardSymbolGroupScaleFactor;
#define DEFAULT_CARDSYMBOL_GROUP_SCALE_FACTOR 0.80

#pragma mark - Properties
-(void)setColor:(UIColor *)color {
    _color = color;
    [self setNeedsDisplay];
}
-(void)setSize:(NSUInteger)size {
    _size = size;
    [self setNeedsDisplay];
}
-(void)setSymbol:(NSUInteger)symbol {
    _symbol = symbol;
    [self setNeedsDisplay];
}

-(UIColor *)fillColor {
    if ([[self shadings][self.shading] isEqualToString:@"solid"]) {
        return self.color;
    } else if ([[self shadings][self.shading] isEqualToString:@"striped"]) {
        return [UIColor colorWithPatternImage:[self stripedPatternForColor:self.color]];
    } else {
        return [UIColor whiteColor];
    }
}

-(UIImage *)stripedPatternForColor:(UIColor *)color {
    if (color == [UIColor greenColor]) {
        return [UIImage imageNamed:@"stripe_green"];
    } else if (color == [UIColor redColor]) {
        return [UIImage imageNamed:@"stripe_red"];
    } else if (color == [UIColor purpleColor]) {
        return [UIImage imageNamed:@"stripe_purple"];
    } else {
        return nil;
    }
}

-(void)handleMatchedCard {
    
}

-(NSArray *)shadings {
    return @[@"solid", @"striped", @"unfilled"];
}

- (id)initWithFrame:(CGRect)frame // designated initializer
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setup];
    }
    return self;
}

-(void)setup {
    self.backgroundColor = nil;
    self.opaque = NO;
    // no opaque + backgroundColor nil = yes transparent
    self.contentMode = UIViewContentModeRedraw; // If my bounds ever change, I want to get my drawRect: called
}

-(void)awakeFromNib { // Not doing alloc init of this view, so I need to setup stuff here
    [self setup];
}

#pragma mark - Drawing methods

-(CGFloat)setCardSymbolGroupScaleFactor {
    if (!_setCardSymbolGroupScaleFactor) {
        _setCardSymbolGroupScaleFactor = DEFAULT_CARDSYMBOL_GROUP_SCALE_FACTOR;
    }
    return _setCardSymbolGroupScaleFactor;
}
-(void)setSetCardSymbolGroupScaleFactor:(CGFloat)setCardSymbolGroupScaleFactor {
    _setCardSymbolGroupScaleFactor = setCardSymbolGroupScaleFactor;
    [self setNeedsDisplay];
}
-(CGFloat)setCardSymbolGroupInsetPointTop {
    return ((1-[self setCardSymbolGroupScaleFactor])/2) * self.bounds.size.height;
}
-(CGFloat)setCardSymbolGroupInsetPointBottom {
    return ([self setCardSymbolGroupScaleFactor] + ((1-[self setCardSymbolGroupScaleFactor])/2)) * self.bounds.size.height;
}
-(CGFloat)setCardSymbolGroupInsetPointLeft {
    return ((1-[self setCardSymbolGroupScaleFactor])/2) * self.bounds.size.width;
}
-(CGFloat)setCardSymbolGroupInsetPointRight {
    return ([self setCardSymbolGroupScaleFactor] + ((1-[self setCardSymbolGroupScaleFactor])/2)) * self.bounds.size.width;
}
-(CGFloat)setCardSymbolScaleFactor {
    return (1.0/3.0) * [self setCardSymbolGroupScaleFactor];
}
-(CGFloat)setCardSymbolHeight {
    return ([self setCardSymbolScaleFactor] * self.bounds.size.height);
}
-(CGFloat)setCardSymbolWidth {
    return ([self setCardSymbolGroupScaleFactor] * self.bounds.size.width);
}


#define CORNER_FONT_STANDARD_HEIGHT 180.0
#define CORNER_RADIUS 12.0

-(CGFloat)cornerScaleFactor { return self.bounds.size.height / CORNER_FONT_STANDARD_HEIGHT; }
-(CGFloat)cornerRadius { return CORNER_RADIUS * [self cornerScaleFactor]; }
-(CGFloat)cornerOffset { return [self cornerRadius] / 3.0; }

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    UIBezierPath *roundedRect = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:[self cornerRadius]]; // as big as the view
    [roundedRect addClip];
    
    [[UIColor whiteColor] setFill]; // sets the color of subsequent fill operations
    UIRectFill(self.bounds); // fills the rectangle (self.bounds)
    [[UIColor blackColor] setStroke];
    [roundedRect stroke];
    
    if (self.symbol == 0) {
        [self drawSquiggle];
    } else if (self.symbol == 1) {
        [self drawOval];
    } else if (self.symbol == 2) {
        [self drawDiamond];
    }
}

-(void)drawSquiggle {
    UIBezierPath *squiggleSymbol = [[UIBezierPath alloc] init];
    [squiggleSymbol moveToPoint:CGPointMake([self setCardSymbolGroupInsetPointLeft],[self setCardSymbolGroupInsetPointTop]+[self setCardSymbolHeight])];
    [squiggleSymbol addCurveToPoint:CGPointMake([self setCardSymbolGroupInsetPointRight], [self setCardSymbolGroupInsetPointTop]) controlPoint1:CGPointMake((self.bounds.size.width/2)-(0.30*[self setCardSymbolWidth]), [self setCardSymbolGroupInsetPointTop]-(0.75*[self setCardSymbolHeight])) controlPoint2:CGPointMake((self.bounds.size.width/2), [self setCardSymbolGroupInsetPointTop] + ([self setCardSymbolHeight]-(0.25*[self setCardSymbolHeight])))];
    [squiggleSymbol addCurveToPoint:CGPointMake([self setCardSymbolGroupInsetPointLeft],[self setCardSymbolGroupInsetPointTop]+[self setCardSymbolHeight]) controlPoint1:CGPointMake((self.bounds.size.width/2)+(0.30*[self setCardSymbolWidth]), [self setCardSymbolGroupInsetPointTop]+(1.75*[self setCardSymbolHeight])) controlPoint2:CGPointMake((self.bounds.size.width/2), [self setCardSymbolGroupInsetPointTop]+(0.25*[self setCardSymbolHeight]))];
    [squiggleSymbol closePath];
    [[self fillColor] setFill];
    [self.color setStroke];
    
    [squiggleSymbol fill];
    [squiggleSymbol stroke];
    
    if (self.size>1) {
        CGAffineTransform translation = CGAffineTransformMakeTranslation(0, [self setCardSymbolHeight]);
        CGPathRef movedPath = CGPathCreateCopyByTransformingPath([squiggleSymbol CGPath], &translation);
        UIBezierPath *secondSquiggleSymbol = [UIBezierPath bezierPathWithCGPath:movedPath];
        [secondSquiggleSymbol fill];
        [secondSquiggleSymbol stroke];
    }
    if (self.size>2) {
        CGAffineTransform translation = CGAffineTransformMakeTranslation(0, [self setCardSymbolHeight]*2);
        CGPathRef movedPath = CGPathCreateCopyByTransformingPath([squiggleSymbol CGPath], &translation);
        UIBezierPath *thirdSquiggleSymbol = [UIBezierPath bezierPathWithCGPath:movedPath];
        [thirdSquiggleSymbol fill];
        [thirdSquiggleSymbol stroke];
    }
    
    
}

-(UIBezierPath *)copyPath:(UIBezierPath *)path toRelativeX:(CGFloat)x andRelativeY:(CGFloat)y {
    UIBezierPath *localPath = [path copy];
    CGAffineTransform translation = CGAffineTransformMakeTranslation(x, y);
    CGPathRef movedPathRef = CGPathCreateCopyByTransformingPath([localPath CGPath], &translation);
    UIBezierPath *movedPath = [UIBezierPath bezierPathWithCGPath:movedPathRef];
    return movedPath;
}

-(void)drawOval {
    UIBezierPath *ovalSymbol = [UIBezierPath bezierPathWithRoundedRect:
                                CGRectMake([self setCardSymbolGroupInsetPointLeft],
                                           [self setCardSymbolGroupInsetPointTop],
                                           [self setCardSymbolWidth],
                                           [self setCardSymbolHeight]) cornerRadius:([self setCardSymbolHeight]/2.0)];
    [[self fillColor] setFill];
    [self.color setStroke];
    
    [ovalSymbol applyTransform:CGAffineTransformMakeScale(1, 0.9)]; // Zodat ze niet tegen elkaar aan zitten
    [ovalSymbol fill];
    [ovalSymbol stroke];
    
    if (self.size > 1) {
        UIBezierPath *secondOvalSymbol = [self copyPath:ovalSymbol toRelativeX:0 andRelativeY:[self setCardSymbolHeight]];
        [secondOvalSymbol fill];
        [secondOvalSymbol stroke];
    }
    if (self.size > 2) {
        UIBezierPath *thirdOvalSymbol = [self copyPath:ovalSymbol toRelativeX:0 andRelativeY:(2*[self setCardSymbolHeight])];
        [thirdOvalSymbol fill];
        [thirdOvalSymbol stroke];
    }
}

-(void)drawDiamond {
    UIBezierPath *diamondSymbol = [[UIBezierPath alloc] init];
    [diamondSymbol moveToPoint:CGPointMake(self.bounds.size.width/2, [self setCardSymbolGroupInsetPointTop])];
    [diamondSymbol addLineToPoint:CGPointMake([self setCardSymbolGroupInsetPointRight], [self setCardSymbolGroupInsetPointTop] + (0.5 * [self setCardSymbolHeight]))];
    [diamondSymbol addLineToPoint:CGPointMake(self.bounds.size.width/2,[self setCardSymbolGroupInsetPointTop] + [self setCardSymbolHeight])];
    [diamondSymbol addLineToPoint:CGPointMake([self setCardSymbolGroupInsetPointLeft], [self setCardSymbolGroupInsetPointTop] + (0.5 * [self setCardSymbolHeight]))];
    [diamondSymbol closePath];
    [diamondSymbol applyTransform:CGAffineTransformMakeScale(1, 0.9)];
    [[self fillColor] setFill];
    [diamondSymbol fill];
    [self.color setStroke];
    [diamondSymbol stroke];
    
    if (self.size > 1) {
        CGAffineTransform translation = CGAffineTransformMakeTranslation(0, [self setCardSymbolHeight]);
        CGPathRef movedPath = CGPathCreateCopyByTransformingPath([diamondSymbol CGPath], &translation);
        UIBezierPath *secondDiamondSymbol = [UIBezierPath bezierPathWithCGPath:movedPath];
        
        [secondDiamondSymbol fill];
        [secondDiamondSymbol stroke];
    }
    if (self.size > 2) {
        CGAffineTransform translation = CGAffineTransformMakeTranslation(0, (2*[self setCardSymbolHeight]));
        CGPathRef movedPath = CGPathCreateCopyByTransformingPath([diamondSymbol CGPath], &translation);
        UIBezierPath *thirdDiamondSymbol = [UIBezierPath bezierPathWithCGPath:movedPath];
        [thirdDiamondSymbol fill];
        [thirdDiamondSymbol stroke];
    }
}

@end
