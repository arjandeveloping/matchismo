//
//  GameSettings.m
//  Matchismo
//
//  Created by Arjan on 06/08/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//

#import "GameSettings.h"

@implementation GameSettings

-(id)alloc {
    [NSException raise:@"Cannot be instantiated!" format:@"Static class 'ClassName' cannot be instantiated"];
    return nil;
}

#define MISMATCH_PENALTY_KEY @"mismatchPenalty"
#define MATCH_BONUS_KEY @"matchBonus"
#define COST_TO_CHOOSE_KEY @"costToChoose"

+(void)setMatchBonus:(NSInteger)matchBonus {
    [[NSUserDefaults standardUserDefaults] setInteger:matchBonus forKey:MATCH_BONUS_KEY];
}
+(void)setMismatchPenalty:(NSInteger)mismatchPenalty {
    [[NSUserDefaults standardUserDefaults] setInteger:mismatchPenalty forKey:MISMATCH_PENALTY_KEY];
}
+(void)setCostToChoose:(NSInteger)costToChoose {
    [[NSUserDefaults standardUserDefaults] setInteger:costToChoose forKey:COST_TO_CHOOSE_KEY];
}

+(NSUInteger)matchBonus {
    if (![[NSUserDefaults standardUserDefaults] integerForKey:MATCH_BONUS_KEY]) {
        [[NSUserDefaults standardUserDefaults] setInteger:4 forKey:MATCH_BONUS_KEY];
    }
    return [[NSUserDefaults standardUserDefaults] integerForKey:MATCH_BONUS_KEY];
}
+(NSUInteger)mismatchPenalty {
    if (![[NSUserDefaults standardUserDefaults] integerForKey:MISMATCH_PENALTY_KEY]) {
        [[NSUserDefaults standardUserDefaults] setInteger:2 forKey:MISMATCH_PENALTY_KEY];
    }
    return [[NSUserDefaults standardUserDefaults] integerForKey:MISMATCH_PENALTY_KEY];
}
+(NSUInteger)costToChoose {
    if (![[NSUserDefaults standardUserDefaults] integerForKey:COST_TO_CHOOSE_KEY]) {
        [[NSUserDefaults standardUserDefaults] setInteger:2 forKey:COST_TO_CHOOSE_KEY];
    }
    return [[NSUserDefaults standardUserDefaults] integerForKey:COST_TO_CHOOSE_KEY];
}

@end
