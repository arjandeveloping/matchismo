//
//  Deck.m
//  Matchismo
//
//  Created by Arjan on 18/07/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//

#import "Deck.h"

@interface Deck ()

@property (strong, nonatomic) NSMutableArray *cards;
// putting it in the implementation file makes it private
// BUT just only declaring a @property makes space ONLY for the pointer itself,
// and does NOT allocate space in the heap for the object the pointer points to.
// The allocation should happen in the Getter (called lazy instantiation)
@end

@implementation Deck

// Getter for private var cards:
-(NSMutableArray *) cards {
    if (!_cards) {
        _cards = [[NSMutableArray alloc] init]; // Lazy instantiation
    }
    return _cards;
}

-(void)addCard:(Card *)card atTop:(BOOL)atTop {
    if (atTop) {
        [self.cards insertObject:card atIndex:0];
    } else {
        [self.cards addObject:card]; // append at the end
    }
}
-(void)addCard:(Card *)card {
    [self addCard:card atTop:NO];
}

-(Card *)drawRandomCard {
    Card *randomCard = nil;
    
    if ([self.cards count]) {
        unsigned index = arc4random() % [self.cards count];
        randomCard = [self.cards objectAtIndex:index];
        [self.cards removeObjectAtIndex:index];
    }
    
    return randomCard;
}

-(NSUInteger)cardCount {
    return [self.cards count];
}


@end
