//
//  SetDeck.m
//  Matchismo
//
//  Created by Arjan on 31/07/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//

#import "SetDeck.h"
#import "SetCard.h"

@interface SetDeck ()

@end

@implementation SetDeck

-(instancetype)init {
    self = [super init];
    for (NSUInteger g = 0; g < [[SetCard validSizes] count]; g++) {
        for (NSUInteger h = 0; h < [[SetCard validSymbols] count]; h++) {
            for (NSUInteger i = 0; i <= [SetCard maxShading]; i++) {
                for (NSUInteger j = 0; j <= [SetCard maxColor]; j++) {
                    SetCard *card = [[SetCard alloc] init];
                    card.size = g;
                    card.symbol = h;
                    card.shading = i;
                    card.color = j;
                    [self addCard:card];
                }
            }
        }
    }
    return self;
}



@end
