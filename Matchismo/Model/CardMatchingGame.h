//
//  CardMatchingGame.h
//  Matchismo
//
//  Created by Arjan on 18/07/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Deck.h"
#import "GameResult.h"

@interface CardMatchingGame : NSObject {
    // _pairSize cannot be accessed from within a subclass, so we expose it here
    NSUInteger _numberOfMatchingCards; // Secure? Yes, by default, it is 'protecred', that is,
    // only accessible to subclasses
}
/* Notice that none of the methods has anything to do with user-interface. it is up t the controller to interpret the Model into somthing presented to the user via the View */

// Public
// Methods
// Designated initializer
-(instancetype)initWithCardCount:(NSUInteger)count usingDeck:(Deck *)deck numberOfPlayers:(NSUInteger)numberOfPlayers;
-(void)chooseCardAtIndex:(NSUInteger)index;
-(Card *)cardAtIndex:(NSUInteger)index;
-(Card *)drawExtraCard;
-(NSUInteger)numberOfCardsInPlay;
-(NSArray *)matchingCombinations;
-(void)switchPlayer;

// Properties
// ReadOnly
@property (nonatomic, readonly) NSInteger currentPlayerScore; // I'm the gamelogic, nobody can tell me what the scorew is, I set the score! Therefore, I'm readonly (publicly)
@property (nonatomic, readonly) NSMutableArray *playerScores; // I'm the gamelogic, nobody can tell me what the scorew is, I set the score! Therefore, I'm readonly (publicly)
@property (nonatomic, readonly) NSUInteger currentPlayer;

@property (nonatomic, readonly) NSInteger lastScore;
@property (nonatomic, readonly) NSMutableArray *lastChosenCards;
@property (nonatomic) NSUInteger mismatchPenalty;
@property (nonatomic) NSUInteger matchBonus;
@property (nonatomic) NSUInteger costToChoose;
// Readonly means that there's no setter, only getter.

// Protected
// for subclasses
// Properties
@property (nonatomic) NSUInteger numberOfMatchingCards; // Abstract

@end
