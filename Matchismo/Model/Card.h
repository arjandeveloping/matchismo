//
//  Card.h
//  Matchismo
//
//  Created by Arjan on 18/07/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Card : NSObject

// Properties
@property (strong, nonatomic) NSString *contents; // Pointer to an object who's class is (or inherits from) NSString

@property (nonatomic) BOOL chosen;
@property (nonatomic) BOOL matched;

// Function declarations
-(int) match: (NSArray *)otherCards;

@end

// Strong means:keep the object that this property points to, in memory until I set this (and all others having a strong pointer) property to nil (zero)
// Weak means: If no one else has a strong pointer to this object, then you can throw it out of memory (e.g 20 weak pointers, 0 strong -> delete it from memory) and set it to nil.