//
//  GameResults.m
//  Matchismo
//
//  Created by Arjan on 06/08/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//

#import "GameResult.h"
@interface GameResult ()

@property (nonatomic) NSInteger key;
@property (nonatomic, readwrite) NSDate *start;
@property (nonatomic, readwrite) NSDate *end;

@end

@implementation GameResult

#define ALL_RESULTS_KEY @"GameResult_All"
#define UNIQUE_KEY @"UniqueKey"
#define START_KEY @"StartDate"
#define END_KEY @"EndDate"
#define SCORE_KEY @"Score"
#define GAME_KEY @"Game"

-(instancetype)init {
    self = [super init];
    if (self) {
        _key = abs(arc4random());
        _start = [NSDate date];
        _end = _start; // zodat ie nooit leeg is.
    }
    return self;
}

-(instancetype)initFromPropertyList:(id)propertyList {
    self = [self init];
    if (self) {
        if ([propertyList isKindOfClass:[NSDictionary class]]) {
            NSDictionary *resultsDictionary = (NSDictionary *)propertyList;
            _key = [resultsDictionary[UNIQUE_KEY] integerValue];
            _start = resultsDictionary[START_KEY];
            _end = resultsDictionary[END_KEY];
            _score = [resultsDictionary[SCORE_KEY] intValue];
            _gameType = resultsDictionary[GAME_KEY];
            if (!_start || !_end) {
                self = nil; // WHY?
            }
        }
    }
    return self;
}

-(NSTimeInterval)duration {
    return [self.end timeIntervalSinceDate:self.start];
}


// Whenever a score changes, we save the result (synchronize)
-(void)setScore:(NSInteger)score {
    _score =  score;
    self.end = [NSDate date]; // update as if now is the end, sice end is readonly
    [self synchronize];
}


// The results are stored in a dictionary, which uses the start time/date
// as key (this way no two results will have the same key). Each entry of
// this dictionary is a property list holding the data of the result;

// ?Property list because duration is not part of the entitites stored??

- (void)synchronize {
    NSMutableDictionary *mutableGameResultsFromUserDefaults = [[[NSUserDefaults standardUserDefaults] dictionaryForKey:ALL_RESULTS_KEY] mutableCopy];
    if (!mutableGameResultsFromUserDefaults) {
        mutableGameResultsFromUserDefaults = [[NSMutableDictionary alloc] init];
    }
    mutableGameResultsFromUserDefaults[FORMAT(@"%ld", self.key)] = [self asPropertyList];
    [[NSUserDefaults standardUserDefaults] setObject:mutableGameResultsFromUserDefaults forKey:ALL_RESULTS_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

// The property list is just another dictionary, created by a helper method
-(id)asPropertyList {
    return @{ UNIQUE_KEY: @(self.key),
              START_KEY: self.start,
              END_KEY: self.end,
              SCORE_KEY: @(self.score),
              GAME_KEY: self.gameType };
}

// When reading back all results, we use another initialization method to
// create each single result, based on the stored property list / dictionary
+(NSArray *)allGameResults {
    NSMutableArray *allGameResults = [[NSMutableArray alloc] init];
    NSArray *allGameResultsFromUserDefaults = [[[NSUserDefaults standardUserDefaults] dictionaryForKey:ALL_RESULTS_KEY] allValues];
    for (id propertyList in allGameResultsFromUserDefaults) {
        GameResult *result = [[GameResult alloc] initFromPropertyList:propertyList];
        [allGameResults addObject:result];
    }
    return allGameResults;
}
+(void)deleteAllGameResults {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:ALL_RESULTS_KEY];
}

-(NSComparisonResult)compareScore:(GameResult *)otherObject {
    return [@(self.score) compare:@(otherObject.score)];
}

-(NSComparisonResult)compareDuration:(GameResult *)otherObject {
    return [@(self.duration) compare:@(otherObject.duration)];
}
-(NSComparisonResult)compareDate:(GameResult *)otherObject {
    return [self.start compare:otherObject.start];
}

@end
