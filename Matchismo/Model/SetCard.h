//
//  SetCard.h
//  Matchismo
//
//  Created by Arjan on 31/07/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//

#import "Card.h"

@interface SetCard : Card
@property (nonatomic) NSUInteger size;
@property (nonatomic) NSUInteger symbol;
@property (nonatomic) NSUInteger shading;
@property (nonatomic) NSUInteger color;

+(NSArray *)validSymbols;
+(NSArray *)validSizes;
+(NSUInteger)maxShading;
+(NSUInteger)maxColor;

@end
