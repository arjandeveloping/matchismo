//
//  Card.m
//  Matchismo
//
//  Created by Arjan on 18/07/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//

#import "Card.h"
@interface Card()
@end

@implementation Card

-(int)match:(NSArray *)otherCards {
    int score = 0;
    for (Card *otherCard in otherCards) {
        if ([otherCard.contents isEqualToString:self.contents]) {
            score = 1;
        }
    }
    return score;
}

@end
