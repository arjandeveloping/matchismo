//
//  PlayingCard.m
//  Matchismo
//
//  Created by Arjan on 18/07/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//

#import "PlayingCard.h"
@interface PlayingCard ()
@end

@implementation PlayingCard
@synthesize suit = _suit; // Because we prvide Getter and Setter

-(NSString *)contents {
    // Even though we are overriding the implementation of the -contents-Method, we are
    // NOT redeclaring the -contents-Property in our header file. That properrty declaration is simply inherited.
    NSArray *rankStrings = [PlayingCard rankStrings];
    return [NSString stringWithFormat:@"%@%@", rankStrings[self.rank], self.suit];
}

-(NSString *)suit {
    return _suit ? _suit : @"?";
}
-(void)setSuit:(NSString *)suit {
    if ([[PlayingCard validSuits] containsObject:suit]) {
        _suit = suit;
    }
}
-(void)setRank:(NSUInteger)rank {
    if (rank <= [PlayingCard maxRank]) {
        _rank = rank;
    }
}

//-(int)match:(NSArray *)otherCards {
//    int score = 0;
//    if ([otherCards count]) {
//        for (PlayingCard *otherCard in otherCards) {
//            if (otherCard.rank == self.rank) {
//                score = 4;
//            } else if ([otherCard.suit isEqualToString:self.suit]) {
//                score = 1;
//            }
//        }
//    }
//    return score;
//}

-(int)match:(NSArray *)otherCards {
    int score = 0;
    NSInteger numOtherCards = [otherCards count];
    
    if (numOtherCards) {
        for (id object in otherCards) {
            if ([object isKindOfClass:[PlayingCard class]]) {
                PlayingCard *otherCard = (PlayingCard *)object;
                if (otherCard.rank == self.rank) {
                    score += 4;
                } else if ([otherCard.suit isEqualToString: self.suit]) {
                    score += 1;
                }
            }
        }
    }
    if (numOtherCards > 1) {
        score += [[otherCards firstObject] match:[otherCards subarrayWithRange:NSMakeRange(1, numOtherCards-1)]];
    }
    return score;
}

//-(int)match:(NSArray *)otherCards {
//    NSMutableArray *cards = [NSMutableArray arrayWithArray:otherCards];
//    [cards addObject:self];
//    
//    int score = 0;
//    int matchingRanks = 0;
//    int matchingSuits = 0;
//    
//    if ([cards count]) {
//        NSMutableArray *combinationsChecked = [[NSMutableArray alloc] init];
//        for (PlayingCard *card in cards) {
//            NSMutableArray *otherCards = [NSMutableArray arrayWithArray:cards];
//            [otherCards removeObject:card];
//            for (PlayingCard *otherCard in otherCards) {
//                bool checkedThisCombination = NO;
//                for (NSArray *array in combinationsChecked) {
//                    if ([array containsObject:card] &&
//                        [array containsObject:otherCard]) {
//                        checkedThisCombination = YES;
//                    }
//                }
//                
//                if (!checkedThisCombination) {
//                    if (otherCard.rank == card.rank) {
//                        matchingRanks += 1;
//                        //score += 4;
//                    } else if ([otherCard.suit isEqualToString:card.suit]) {
//                        matchingSuits += 1;
//                        //score += 1;
//                    }
//                    NSArray *combination = @[card, otherCard];
//                    [combinationsChecked addObject:combination];
//                }
//            }
//        }
//    }
//    
//    double matchingRanksRatio = (double) matchingRanks / [cards count];
//    double matchingSuitsRatio = (double) matchingSuits / [cards count];
//    
//    score += matchingRanksRatio * 40;
//    score += matchingSuitsRatio * 10;
//    
//    return score;
//}

+(NSArray *)validSuits {
    return @[@"♠︎",@"♣︎",@"♥︎",@"♦︎"]; //
}
+(NSArray *)rankStrings {
    return @[@"?",@"A",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"J",@"Q",@"K"];
}
+(NSUInteger)maxRank {
    return [PlayingCard rankStrings].count-1;
}

@end
