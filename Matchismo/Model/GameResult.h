//
//  GameResults.h
//  Matchismo
//
//  Created by Arjan on 06/08/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameResult : NSObject

+(NSArray *)allGameResults; // of GameResults
+(void)deleteAllGameResults;

@property (nonatomic, readonly) NSDate *start;
@property (nonatomic, readonly) NSDate *end;
@property (readonly, nonatomic) NSTimeInterval duration;
@property (nonatomic) NSInteger score;
@property (strong, nonatomic) NSString *gameType;

-(NSComparisonResult)compareScore:(GameResult *)result;
-(NSComparisonResult)compareDuration:(GameResult *)result;
-(NSComparisonResult)compareDate:(GameResult *)result;
@end
