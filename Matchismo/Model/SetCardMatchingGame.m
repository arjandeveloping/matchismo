//
//  SetCardMatchingGame.m
//  Matchismo
//
//  Created by Arjan on 01/08/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//

#import "SetCardMatchingGame.h"

@implementation SetCardMatchingGame

-(NSUInteger)numberOfMatchingCards {
    if (_numberOfMatchingCards < 3) {
        _numberOfMatchingCards = 3;
    }
    return _numberOfMatchingCards;
}

-(NSArray *)matchingCombinations {
    NSMutableArray *combinations = [[NSMutableArray alloc] init];
    //NSArray *cards = self.cards;
    //NSArray *cards = @[@1,@2,@3,@4,@5];
    for (NSInteger i = 0; i < [self numberOfCardsInPlay]; i++) {
        for (NSInteger j = i+1; j < [self numberOfCardsInPlay]; j++) {
            for (NSInteger k = j+1; k < [self numberOfCardsInPlay]; k++) {
                Card *card1 = [self cardAtIndex:i];
                Card *card2 = [self cardAtIndex:j];
                Card *card3 = [self cardAtIndex:k];
                if (!card1.matched && !card2.matched && !card3.matched) {
                    if ([card1 match:@[card2,card3]]) {
                        NSArray *newCombination = @[[NSNumber numberWithInteger:i], [NSNumber numberWithInteger:j], [NSNumber numberWithInteger:k]];
                        [combinations addObject:newCombination];
                        //NSLog(@"%@", newCombination);
                    }
                }
            }
        }
    }
    return combinations;
}
@end
