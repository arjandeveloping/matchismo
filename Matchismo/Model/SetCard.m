//
//  SetCard.m
//  Matchismo
//
//  Created by Arjan on 31/07/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//

#import "SetCard.h"

@interface SetCard ()

@end

@implementation SetCard

-(NSString *)contents {
    // Even though we are overriding the implementation of the -contents-Method, we are
    // NOT redeclaring the -contents-Property in our header file. That properrty declaration is simply inherited.
    NSString *str = [NSString stringWithFormat:@"%lu-%@-%lu-%lu", (unsigned long)self.size, [SetCard validSymbols][self.symbol], (unsigned long)self.shading, (unsigned long)self.color];
    return str;
//    return nil;
}

-(int)match:(NSArray *)otherCards {
    int score = 0;
    bool allTheSameOrAllDifferentSizes = ([self allTheSameSize:otherCards] ||
                                          [self allDifferentSize:otherCards]);
    bool allTheSameOrAllDifferentSymbols = ([self allTheSameSymbol:otherCards] ||
                                            [self allDifferentSymbol:otherCards]);
    bool allTheSameOrAllDifferentShadings = ([self allTheSameShading:otherCards] ||
                                            [self allDifferentShading:otherCards]);
    bool allTheSameOrAllDifferentColors = ([self allTheSameColor:otherCards] ||
                                            [self allDifferentColor:otherCards]);
    if (allTheSameOrAllDifferentSizes &&
        allTheSameOrAllDifferentSymbols &&
        allTheSameOrAllDifferentShadings &&
        allTheSameOrAllDifferentColors)
        score = 4;

    return score;
}

-(BOOL)allTheSameSize:(NSArray *)otherCards {
    bool allTheSame = NO;
    NSUInteger numOtherCards = [otherCards count];
    SetCard *otherCard = [otherCards firstObject];
    if (numOtherCards == 1) {
        return otherCard.size == self.size;
    } else if (numOtherCards > 1) {
        allTheSame = otherCard.size == self.size && [self allTheSameSize:[otherCards subarrayWithRange:NSMakeRange(1, numOtherCards-1)]];
    }
    return allTheSame;
}
-(BOOL)allTheSameSymbol:(NSArray *)otherCards {
    bool allTheSame = NO;
    NSUInteger numOtherCards = [otherCards count];
    SetCard *otherCard = [otherCards firstObject];
    if (numOtherCards == 1) {
      return otherCard.symbol == self.symbol;
    } else if (numOtherCards > 1) {
      allTheSame = otherCard.symbol == self.symbol && [self allTheSameSymbol:[otherCards subarrayWithRange:NSMakeRange(1, numOtherCards-1)]];
    }
    return allTheSame;
}
-(BOOL)allTheSameShading:(NSArray *)otherCards {
    bool allTheSame = NO;
    NSUInteger numOtherCards = [otherCards count];
    SetCard *otherCard = [otherCards firstObject];
    if (numOtherCards == 1) {
        return otherCard.shading == self.shading;
    } else if (numOtherCards > 1) {
        allTheSame = otherCard.shading == self.shading && [self allTheSameShading:[otherCards subarrayWithRange:NSMakeRange(1, numOtherCards-1)]];
    }
    return allTheSame;
}
-(BOOL)allTheSameColor:(NSArray *)otherCards {
    bool allTheSame = NO;
    NSUInteger numOtherCards = [otherCards count];
    SetCard *otherCard = [otherCards firstObject];
    if (numOtherCards == 1) {
        return otherCard.color == self.color;
    } else if (numOtherCards > 1) {
        allTheSame = otherCard.color == self.color && [self allTheSameColor:[otherCards subarrayWithRange:NSMakeRange(1, numOtherCards-1)]];
    }
    return allTheSame;
}
-(BOOL)allDifferentSize:(NSArray *)otherCards {
    bool allDifferent = YES;
    NSUInteger numOtherCards = [otherCards count];
    if (numOtherCards) {
        for (id object in otherCards) {
            if ([object isKindOfClass:[SetCard class]]) {
                SetCard *otherCard = (SetCard *)object;
                if (otherCard.size == self.size)
                    return NO;
            }
        }
    }
    if (numOtherCards > 1) {
        allDifferent = [[otherCards firstObject] allDifferentSize:[otherCards subarrayWithRange:NSMakeRange(1, numOtherCards-1)]];
    }
    return allDifferent;
}
-(BOOL)allDifferentSymbol:(NSArray *)otherCards {
    bool allDifferent = YES;
    NSUInteger numOtherCards = [otherCards count];
    if (numOtherCards) {
        for (id object in otherCards) {
            if ([object isKindOfClass:[SetCard class]]) {
                SetCard *otherCard = (SetCard *)object;
                if (otherCard.symbol == self.symbol)
                    return NO;
            }
        }
    }
    if (numOtherCards > 1) {
        allDifferent = [[otherCards firstObject] allDifferentSymbol:[otherCards subarrayWithRange:NSMakeRange(1, numOtherCards-1)]];
    }
    return allDifferent;
}
-(BOOL)allDifferentShading:(NSArray *)otherCards {
    bool allDifferent = YES;
    NSUInteger numOtherCards = [otherCards count];
    if (numOtherCards) {
        for (id object in otherCards) {
            if ([object isKindOfClass:[SetCard class]]) {
                SetCard *otherCard = (SetCard *)object;
                if (otherCard.shading == self.shading)
                    return NO;
            }
        }
    }
    if (numOtherCards > 1) {
        allDifferent = [[otherCards firstObject] allDifferentShading:[otherCards subarrayWithRange:NSMakeRange(1, numOtherCards-1)]];
    }
    return allDifferent;
}
-(BOOL)allDifferentColor:(NSArray *)otherCards {
    bool allDifferent = YES;
    NSUInteger numOtherCards = [otherCards count];
    if (numOtherCards) {
        for (id object in otherCards) {
            if ([object isKindOfClass:[SetCard class]]) {
                SetCard *otherCard = (SetCard *)object;
                if (otherCard.color == self.color)
                    return NO;
            }
        }
    }
    if (numOtherCards > 1) {
        allDifferent = [[otherCards firstObject] allDifferentColor:[otherCards subarrayWithRange:NSMakeRange(1, numOtherCards-1)]];
    }
    return allDifferent;
}

+(NSArray *)validSymbols {
    return @[@"▲",@"◼︎",@"●"];
}
+(NSArray *)validSizes {
    return @[@1,@2,@3];
}
+(NSUInteger)maxShading {
    return 2;
}
+(NSUInteger)maxColor {
    return 2;
}

//ALTERNATIEF:
//- (int)match:(NSArray *)otherCards
//{
//    int score = 0;
//    if ([otherCards count] == self.numberOfMatchingCards - 1) {
//        NSMutableArray *colors = [[NSMutableArray alloc] init];
//        NSMutableArray *symbols = [[NSMutableArray alloc] init];
//        NSMutableArray *shadings = [[NSMutableArray alloc] init];
//        NSMutableArray *numbers = [[NSMutableArray alloc] init];
//        [colors addObject:self.color];
//        [symbols addObject:self.symbol];
//        [shadings addObject:self.shading];
//        [numbers addObject:@(self.number)];
//        for (id otherCard in otherCards) {
//            if ([otherCard isKindOfClass:[SetCard class]]) {
//                SetCard *otherSetCard = (SetCard *)otherCard;
//                if (![colors containsObject:otherSetCard.color])
//                    [colors addObject:otherSetCard.color];
//                if (![symbols containsObject:otherSetCard.symbol])
//                    [symbols addObject:otherSetCard.symbol];
//                if (![shadings containsObject:otherSetCard.shading])
//                    [shadings addObject:otherSetCard.shading];
//                if (![numbers containsObject:@(otherSetCard.number)])
//                    [numbers addObject:@(otherSetCard.number)];
//                if (([colors count] == 1 || [colors count] == self.numberOfMatchingCards)
//                    && ([symbols count] == 1 || [symbols count] == self.numberOfMatchingCards)
//                    && ([shadings count] == 1 || [shadings count] == self.numberOfMatchingCards)
//                    && ([numbers count] == 1 || [numbers count] == self.numberOfMatchingCards)) {
//                    score = 4;
//                }
//            }
//        }
//    }
//    return score;
//}

-(NSString *)description {
    return FORMAT(@"Color: %lu Size: %lu Shading: %lu Symbol: %lu", self.color, (unsigned long)self.size, (unsigned long)self.shading, (unsigned long)self.symbol);
}
@end
