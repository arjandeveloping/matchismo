//
//  PlayingCardDeck.h
//  Matchismo
//
//  Created by Arjan on 18/07/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//

#import "Deck.h"

@interface PlayingCardDeck : Deck

@end
