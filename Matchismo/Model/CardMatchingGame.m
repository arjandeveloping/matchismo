//
//  CardMatchingGame.m
//  Matchismo
//
//  Created by Arjan on 18/07/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//

#import "CardMatchingGame.h"

@interface CardMatchingGame ()
@property (nonatomic, readwrite) NSInteger currentPlayerScore;
/* With this we redeclare the score to not be readonly within the implementation only */

@property (nonatomic, readwrite) NSMutableArray *playerScores; // Of NSNumber
@property (nonatomic, readwrite) NSUInteger currentPlayer;

@property (nonatomic, readwrite) NSInteger lastScore;
@property (nonatomic, strong) NSMutableArray *lastChosenCards;
@property (nonatomic) NSUInteger numberOfPlayers;

@property (nonatomic, strong) NSMutableArray *cards; // of Card

@property (nonatomic, strong) Deck *deck; // Needed to draw extra cards after init
@end

@implementation CardMatchingGame

-(void)switchPlayer {
    if (self.currentPlayer == self.numberOfPlayers-1) {
        self.currentPlayer = 0;
    } else { self.currentPlayer++; }
}

-(NSUInteger)currentPlayer {
    if (_currentPlayer < self.numberOfPlayers) {
        return _currentPlayer;
    } return NSNotFound;
}

-(NSMutableArray *)playerScores {
    if (!_playerScores) {
        _playerScores = [[NSMutableArray alloc] init];
        for (int i = 0; i < self.numberOfPlayers; i++) {
            [_playerScores addObject:@0];
        }
    }
    return _playerScores;
}

-(void)setCurrentPlayerScore:(NSInteger)currentPlayerScore {
    self.playerScores[self.currentPlayer] = [NSNumber numberWithInteger:currentPlayerScore];
}
-(NSInteger)currentPlayerScore {
    NSNumber *numScore;
    id score = self.playerScores[self.currentPlayer];
    if ([score isKindOfClass:[NSNumber class]]) {
        numScore = (NSNumber *)score;
    }
    return [numScore integerValue];
}

-(NSArray *)matchingCombinations {
    return nil; // Abstract
}

-(NSMutableArray *) cards{
    if (!_cards) {
        _cards = [[NSMutableArray alloc] init];
    }
    return _cards;
}
-(NSUInteger)numberOfMatchingCards { // Abstract
    return NSNotFound;
//    if (_pairSize < 2) {
//        _pairSize = 2;
//    }
//    return _pairSize;
}

-(instancetype)initWithCardCount:(NSUInteger)count usingDeck:(Deck *)deck numberOfPlayers:(NSUInteger)numberOfPlayers{
    self = [super init];
    
    if (self) {
        self.deck = deck;
        for (int i = 0; i < count; i++) {
            Card *card = [deck drawRandomCard];
            // Adding nil to an NSMutableArray will crash the program
            if (card) {
                [self.cards addObject:card];
            } else {
                // quit!
                self = nil;
                break;
            }
        }
        self.numberOfPlayers = numberOfPlayers;
    }
    
    return self;
}

-(Card *)drawExtraCard {
    Card *crd = [self.deck drawRandomCard];
    if (crd) {
        [self.cards addObject:crd];
    }    
    return crd;
}

-(Card *)cardAtIndex:(NSUInteger)index {
    /* Let's be sure to check if the argument is within bounds */
    return index < [self.cards count] ? self.cards[index] : nil;
}

-(NSUInteger)numberOfCardsInPlay{
    return [self.cards count];
}

-(void)chooseCardAtIndex:(NSUInteger)index {
    Card *card = [self cardAtIndex:index];
    
    if (card) { // if not nil
        if (!card.matched) { // not matched yet
            if (card.chosen) {
                card.chosen = NO; // toggle chosen state
                for (Card *tcard in self.cards) {
                    if (!tcard.chosen) {
                        [self.lastChosenCards removeObject:tcard];
                    }
                }
            } else {
                NSMutableArray *chosenOtherCards = [[NSMutableArray alloc] init];
                for (Card *otherCard in self.cards) {
                    if (otherCard.chosen && !otherCard.matched)
                        [chosenOtherCards addObject:otherCard];
                }
                NSArray *chosenCards = [chosenOtherCards arrayByAddingObject:card];
                
                self.lastScore = 0;
                self.lastChosenCards = [chosenOtherCards mutableCopy];
                [self.lastChosenCards addObject:card];
                
                if (chosenCards.count == self.numberOfMatchingCards) {
                    // match against other cards that are unmatched yet
                    //, but are chosen, which is only one card
                    //int matchScore = [card match:chosenOtherCards];
                    int matchScore = [card match:chosenOtherCards];
                    
                    if (matchScore) {
                        self.lastScore = matchScore * self.matchBonus;
                        for (Card *chosenCard in chosenCards) {
                            chosenCard.matched = YES;
                        }
                     } else {
                         self.lastScore = -self.mismatchPenalty;
                        // mismatch, UNchoose other chosen cards
                        for (Card *chosenCard in chosenCards) {
                            chosenCard.chosen = NO;
                        }
                      }
                } else { // not yet numberOfMatchingCards
                }
                
                self.currentPlayerScore +=self.lastScore;
                //self.score += self.lastScore;
                //self.score -= self.costToChoose;
                self.currentPlayerScore -= self.costToChoose;
                card.chosen = YES;
            }
        }
    }
}
@end