//
//  GameSettings.h
//  Matchismo
//
//  Created by Arjan on 06/08/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//
// Static class

#import <Foundation/Foundation.h>

@interface GameSettings : NSObject
+(void)setMatchBonus:(NSInteger)matchBonus;
+(void)setMismatchPenalty:(NSInteger)mismatchPenalty;
+(void)setCostToChoose:(NSInteger)costToChoose;

+(NSUInteger)matchBonus;
+(NSUInteger)mismatchPenalty;
+(NSUInteger)costToChoose;
@end
