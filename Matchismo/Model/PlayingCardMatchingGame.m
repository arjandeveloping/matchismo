//
//  PlayingCardMatchingGame.m
//  Matchismo
//
//  Created by Arjan on 31/07/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//

#import "PlayingCardMatchingGame.h"

@implementation PlayingCardMatchingGame

-(NSUInteger)numberOfMatchingCards {
    if (_numberOfMatchingCards < 2) {
        _numberOfMatchingCards = 2;
    }
    return _numberOfMatchingCards;
}

-(NSArray *)matchingCombinations {
    NSMutableArray *combinations = [[NSMutableArray alloc] init];
    //NSArray *cards = self.cards;
    //NSArray *cards = @[@1,@2,@3,@4,@5];
    for (NSInteger i = 0; i < self.numberOfCardsInPlay; i++) {
        for (NSInteger j = i+1; j < self.numberOfCardsInPlay; j++) {
            Card *card1 = [self cardAtIndex:i];
            Card *card2 = [self cardAtIndex:j];
            if (!card1.matched && !card2.matched) {
                NSArray *newCombination = @[[NSNumber numberWithInteger:i], [NSNumber numberWithInteger:j]];
                [combinations addObject:newCombination];
                //NSLog(@"%@", newCombination);
            }
        }
    }
    return combinations;
}
@end
