//
//  CardGameHighScoreViewController.m
//  Matchismo
//
//  Created by Arjan on 05/08/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//

#import "CardGameStatisticsViewController.h"
#import "GameResult.h"

@interface CardGameStatisticsViewController ()
@property (weak, nonatomic) IBOutlet UITextView *gameStatisticsTextView;
@property (strong, nonatomic) NSArray *results; // Do this, so you have a "model" for this MVC
// to let it work kind of independently without having a web of connections
@property (weak, nonatomic) IBOutlet UISegmentedControl *selectedSortSegment;
@end

@implementation CardGameStatisticsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.results = [GameResult allGameResults];
    [self.selectedSortSegment sendActionsForControlEvents:UIControlEventValueChanged]; // will update UI automatically
    //[self updateUI];
}
- (IBAction)touchDeleteAllGameStatisticsButton {
    [GameResult deleteAllGameResults];
    self.results = [GameResult allGameResults];
    [self updateUI];
}

-(void)updateUI {
    NSMutableString *string = [[NSMutableString alloc] init];

    NSString *headline = @"No scores yet to display";
    if ([self.results count]) {
        headline = @"Date";
        headline = [headline stringByPaddingToLength:16 withString: @" " startingAtIndex:0];
        headline = [headline stringByAppendingString: @"Duration"];
        headline = [headline stringByPaddingToLength:26 withString: @" " startingAtIndex:0];
        headline = [headline stringByAppendingString: @"Score"];
        headline = [headline stringByPaddingToLength:33 withString: @" " startingAtIndex:0];
        headline = [headline stringByAppendingString: @"Game"];
    }
    [string appendString:headline];
    [string appendString:@"\n"];
    

//    NSArray *allGameStatisticsSortedByScore = [allGameStatistics sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
//        NSNumber *first = a[2];
//        NSNumber *second = b[2];
//        return [first compare:second];
//    }];
    
    for (id object in self.results) {
        if ([object isKindOfClass:[GameResult class]]) {
            GameResult *gameResult = (GameResult *)object;
            [string appendString: [self stringFromGameResult:gameResult]];
            [string appendString:@"\n"];
        }
    }
    [self.gameStatisticsTextView setText:string];
    
    NSArray *sortedResults = [self.results sortedArrayUsingSelector:@selector(compareScore:)];
    NSRange range = [self.gameStatisticsTextView.text rangeOfString:headline];
    [self.gameStatisticsTextView.textStorage addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Courier-Bold" size:11] range:range];
    [self changeColorInGameStatisticsTextViewOfResult:[sortedResults firstObject] toColor:[UIColor redColor]];
    [self changeColorInGameStatisticsTextViewOfResult:[sortedResults lastObject] toColor:[UIColor greenColor]];
    
}

-(NSString *)stringFromGameResult:(GameResult *)gameResult {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd-MM-yy HH:mm";
    
    int secondsPlayed = (int)lroundl(gameResult.duration);
    int hoursPlayed = secondsPlayed / 3600;
    int minutesLeft = (secondsPlayed % 3600) / 60;
    int secondsLeft = (secondsPlayed % 3600) % 60;
    NSString *startDateString = [dateFormatter stringFromDate:gameResult.start];
    NSString *line = FORMAT(@"%@", startDateString);
    line = [line stringByPaddingToLength:16 withString: @" " startingAtIndex:0];
    line = [line stringByAppendingString: FORMAT(@"%dh%dm%ds", hoursPlayed, minutesLeft, secondsLeft)];
    line = [line stringByPaddingToLength:26 withString: @" " startingAtIndex:0];
    line = [line stringByAppendingString: FORMAT(@"%ld", (long)gameResult.score)];
    line = [line stringByPaddingToLength:33 withString: @" " startingAtIndex:0];
    line = [line stringByAppendingString: FORMAT(@"%@", gameResult.gameType)];
    
    return line;
}

-(void)changeColorInGameStatisticsTextViewOfResult:(GameResult *)result toColor:(UIColor *)color {
    NSRange range = [self.gameStatisticsTextView.text rangeOfString:[self stringFromGameResult:result]];
    [self.gameStatisticsTextView.textStorage addAttribute:NSForegroundColorAttributeName value:color range:range];
}

- (IBAction)selectedSortSegmentValueChanged:(UISegmentedControl *)sender {
    switch (sender.selectedSegmentIndex) {
        case 0: // Date
            self.results = [self.results sortedArrayUsingSelector:@selector(compareDate:)];
            break;
        case 1: // Score
            self.results = [self.results sortedArrayUsingSelector:@selector(compareScore:)];
            break;
        case 2: // Duration
            self.results = [self.results sortedArrayUsingSelector:@selector(compareDuration:)];
        default:
            break;
    }
    [self updateUI];
}

@end
