//
//  CardGameViewController.h
//  Matchismo
//
//  Created by Arjan on 18/07/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//
// Abstract class. Must implement methods as described below.

#import <UIKit/UIKit.h>
#import "Deck.h"
#import "CardMatchingGame.h"
#import "Grid.h"
#import "CardView.h"
#import <AudioToolbox/AudioToolbox.h>

@interface CardGameViewController : UIViewController
// Public
-(void)updateUI;
-(BOOL)addCards:(NSUInteger)n;
-(IBAction)newGameButton;

// Protected
// for subclasses
// Methods
-(Deck *)createDeck; // abstract
-(CardMatchingGame *)createGameUsingDeck:(Deck *)deck; // abstract
-(CardView *)createViewForCard:(Card *)card; // Abstract
-(void)handleMatchedCard:(CardView *)cardView; // Abstract
-(void)resetGame; // Soort van Abstract eerst subclass' resetGame dan deze
-(void)checkForValidCombinations;

// Any abstract method, you gotta make public
// Otherwise the subclasses don't know that they're
// supposed to implement them


// Properties
@property (strong, nonatomic) NSString *gameType;
@property (nonatomic) NSUInteger cardCount; // Abstract
@property (nonatomic) NSUInteger numberOfStartingCards; // Abstract

@property (strong, nonatomic) CardMatchingGame *game;
@property (nonatomic, strong) Grid *grid;

@property (strong, nonatomic) NSMutableArray *cardViews; // Of UIView
@property (nonatomic) CGSize maxCardSize;
@end
