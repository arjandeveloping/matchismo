//
//  CardGameViewController.m
//  Matchismo
//
//  Created by Arjan on 18/07/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//

#import "CardGameViewController.h"
#import "CardMatchingGame.h"
#import "GameResult.h"
#import "GameSettings.h"
#import "CardsContainerView.h"
#import "CardView.h"

#define playWinningSound AudioServicesPlaySystemSound(1036)

@interface CardGameViewController ()
@property (weak, nonatomic) IBOutlet CardsContainerView *cardsContainerView;
@property (weak, nonatomic) IBOutlet UILabel *flipsLabel;
// An OUTLET is a connection from the Controller to the View
// An ACTION is a a connection from the View to the Controller */
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (strong, nonatomic) NSMutableArray *gameResults; // Of GameResult
@property (strong, nonatomic) UIDynamicAnimator *animator;
@property (strong, nonatomic) UIAlertView *gameOverAlertView;
@end


@implementation CardGameViewController

#pragma mark - Initialization
-(void)viewDidLoad {
    [super viewDidLoad];
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.maxCardSize = CGSizeMake(80.0, 120.0);
    self.cardsContainerView.grid = self.grid;
    
}
-(void)viewDidAppear:(BOOL)animated {
    [self updateUI];
}

#pragma mark - Properties
-(UIAlertView *)gameOverAlertView {
    if (!_gameOverAlertView) {
        _gameOverAlertView = [[UIAlertView alloc] initWithTitle:@"No more sets" message:@"No more sets in the current card stack" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
    return _gameOverAlertView;
}

-(NSMutableArray *)gameResults {
    if (!_gameResults) {
        _gameResults = [[NSMutableArray alloc] init];
        for (int i = 0; i < [self.game.playerScores count]; i++) {
            GameResult *gameResult = [[GameResult alloc] init];
            gameResult.gameType = self.gameType; // Lazily set also the game type
            [_gameResults addObject:gameResult];
        }
    }
    return _gameResults;
}

-(CardMatchingGame *)game {
    if (!_game) {
        _game = [self createGameUsingDeck:[self createDeck]];
        _game.matchBonus = [GameSettings matchBonus];
        _game.mismatchPenalty = [GameSettings mismatchPenalty];
        _game.costToChoose = [GameSettings costToChoose];
    }
    return _game;
}
-(Grid *)grid {
    if (!_grid) {
        _grid = [[Grid alloc] init];
        [_grid setCellAspectRatio:0.66];
        [_grid setSize:self.cardsContainerView.bounds.size];
        [_grid setMaxCellWidth:self.maxCardSize.width];
        [_grid setMaxCellHeight:self.maxCardSize.height];
        [_grid setMinimumNumberOfCells:self.game.numberOfCardsInPlay];
    }
    return _grid;
}
- (UIDynamicAnimator *)animator {
    if (!_animator) {
        _animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.cardsContainerView];
    }
    return _animator;
}
-(NSMutableArray *)cardViews {
    if (!_cardViews) {
        _cardViews = [NSMutableArray arrayWithCapacity:self.numberOfStartingCards];
    }
    return _cardViews;
}

#pragma mark - Methods
const double cAnimationDelay = 0.1;
-(void)addCardViewsAnimated:(NSArray *)cardViews {
    NSArray *localCardViews = [cardViews copy];
    if ([localCardViews count]) {
        int index = 1;
        for (CardView *cardView1 in localCardViews) {
            CGRect frame = cardView1.frame;
            if (![self.cardsContainerView.subviews containsObject:cardView1]) {
                [self.cardsContainerView addSubview:cardView1];
                [cardView1 setCenter:CGPointMake(self.view.center.x, self.view.frame.size.height + cardView1.frame.size.height)];
            }
            [UIView animateWithDuration:0.1 delay:index*cAnimationDelay options:UIViewAnimationOptionCurveLinear animations:^{
                cardView1.frame = frame;
            } completion:^(BOOL finished){
                //
            }];
            index++;
        }
    } else { // done
        //[self checkForValidCombinations];
    }
}

-(void)checkForValidCombinations {
    if (![[self.game matchingCombinations] count]) {
        if (!self.gameOverAlertView.visible) {
            [self.gameOverAlertView show];
        }
    }
}

-(void)resetGame {
    //self.gameResult = nil;
    self.gameResults = nil;
    self.game = nil;
    self.cardViews = nil;
    self.grid = nil;
    [[self.cardsContainerView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self updateUI];
}

-(IBAction)newGameButton {
    [self resetGame];
}

-(BOOL)addCards:(NSUInteger)n {
    for (CardView *cardView in self.cardsContainerView.subviews) {
        [cardView removeGestureRecognizer:cardView.tapGestureRecognizer];
        [cardView removeGestureRecognizer:cardView.panGestureRecognizer];
        [self.animator removeBehavior:cardView.attachmentBehavior];
    }
    for (int i = 0; i < n; i++) {
        Card *newCard = [self.game drawExtraCard];
        if (newCard) {
            CardView* newCardView = [self createViewForCard:newCard];
            [self.cardViews addObject:newCardView]; // add it to the array of CardViews
            [self.cardsContainerView addSubview:newCardView];
            [self.grid setMinimumNumberOfCells:[self.grid minimumNumberOfCells]+1];
        } else {
            return NO;
        }
    }
    [self.cardsContainerView setNeedsDisplay];
    return YES;
}

-(void)updateUI {
    UIDeviceOrientation deviceOrientation = [[UIDevice currentDevice] orientation];
    if (deviceOrientation == UIDeviceOrientationUnknown) {
        //return;
    }
    __weak CardGameViewController *weakSelf = self;
    NSMutableArray *cardViewsToAnimate = [[NSMutableArray alloc] init];
    for (NSUInteger cardIndex = 0; cardIndex < self.game.numberOfCardsInPlay; cardIndex++) {
        Card *card = [self.game cardAtIndex:cardIndex];
        CardView *cardView;
        if (cardIndex < [self.cardViews count]) { // cardView bestaat
            cardView = [self.cardViews objectAtIndex:cardIndex];
            bool onScreen = [self.cardsContainerView.subviews containsObject:cardView];
            if (onScreen && card.matched) {
                [self handleMatchedCard:cardView]; // Animate faceUp, delay, and removal of the card. Then remove from superView
                CGRect newCardFrame = cardView.frame;
                [UIView animateWithDuration:0.7 delay:1.0 options:UIViewAnimationOptionCurveLinear animations:^{
                    cardView.alpha = 0.0;
                } completion:^(BOOL finished){
                    if (finished) {
                        [cardView removeFromSuperview];
                        Card *newCard = [self.game drawExtraCard];
                        if (newCard) { // If there is another card left
                            CardView *newCardView = [weakSelf createViewForCard:newCard]; // Create a new CardView for this new Card
                            newCardView.tag = cardIndex;
                            newCardView.frame = newCardFrame;
                            [weakSelf.cardViews addObject:newCardView]; // add it to the array of CardViews
                            [self addCardViewsAnimated:@[newCardView]];
                        } else {
                            [self checkForValidCombinations];
                            //[self.cardsContainerView setNeedsDisplay]; // juist niet!
                        }
                    } else {
                        NSLog(@"fadeout called before finish!");
                    }
                }];
            }
        } else { // cardView bestaat nog niet - alleen bij Redeal
            cardView = [self createViewForCard:card];
            cardView.tag = cardIndex;
            [self.cardViews addObject:cardView];
            // animate in
            CGRect frame = [self.grid frameOfCellAtRow:cardIndex / self.grid.columnCount inColumn:cardIndex % self.grid.columnCount]; // so much wow
            cardView.frame = frame;
            [cardViewsToAnimate addObject:cardView];
        }
    }
    
    if ([cardViewsToAnimate count]) {
        [self addCardViewsAnimated:cardViewsToAnimate];
    }
    
    self.scoreLabel.text = [NSString stringWithFormat:@"Score: %ld", (long)self.game.currentPlayerScore];
    //self.gameResult.score = self.game.currentPlayerScore; // also sets end datetime // doesnt work anymore of course with multiple players per game, since now we can have multiple players
    if (self.game.currentPlayer < [self.gameResults count]) {
        id obj = self.gameResults[self.game.currentPlayer];
        if ([obj isKindOfClass:[GameResult class]]) {
            GameResult *gameResult = (GameResult *)obj;
            gameResult.score = self.game.currentPlayerScore; // also sets end datetime
        }
    }
}

#pragma mark - UIGestureRecognizer handlers

-(void)tap:(UITapGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateRecognized) {
        [UIView animateWithDuration:0.2 animations:^{
            [self.cardsContainerView resetGrid];
        }];
        for (CardView *cardView in self.cardsContainerView.subviews) {
            [cardView removeGestureRecognizer:cardView.tapGestureRecognizer];
            [cardView removeGestureRecognizer:cardView.panGestureRecognizer];
            [self.animator removeBehavior:cardView.attachmentBehavior];
        }
    }
}

-(IBAction)pan:(UIPanGestureRecognizer *)sender {
    if ([sender.view isKindOfClass:[CardView class]]) {
        CGPoint gesturePoint = [sender locationInView:self.cardsContainerView];
        if (sender.state == UIGestureRecognizerStateBegan) {
            for (CardView *cardView in self.cardsContainerView.subviews) {
                if (!cardView.attachmentBehavior) {
                    cardView.attachmentBehavior =  [[UIAttachmentBehavior alloc] initWithItem:cardView
                                                                             attachedToAnchor:self.cardsContainerView.center];
                } else {
                    cardView.attachmentBehavior.anchorPoint = gesturePoint;
                    
                }
                if (![self.animator.behaviors containsObject:cardView.attachmentBehavior]) {
                    [self.animator addBehavior:cardView.attachmentBehavior];
                }
            
                if (![cardView.gestureRecognizers containsObject:cardView.tapGestureRecognizer]) {
                    [cardView.tapGestureRecognizer addTarget:self action:@selector(tap:)];
                    [cardView addGestureRecognizer:cardView.tapGestureRecognizer];
                }
            }
        } else if (sender.state == UIGestureRecognizerStateChanged) {
            for (CardView *cardView in self.cardsContainerView.subviews) {
                cardView.attachmentBehavior.anchorPoint = gesturePoint;
            }
        } else if (sender.state == UIGestureRecognizerStateEnded) {
            for (CardView *cardView in self.cardsContainerView.subviews) {
                [self.animator removeBehavior:cardView.attachmentBehavior];
            }
        }
    }
}

-(IBAction)pinch:(UIPinchGestureRecognizer *)sender {
    __block CGPoint midpoint = [sender locationInView:self.cardsContainerView];

    
    if (sender.state == UIGestureRecognizerStateRecognized) {
        for (CardView *cardView in self.cardsContainerView.subviews) {
            [UIView animateWithDuration:0.5 animations:^{
                int x = 4 - ((float)arc4random() / 0x100000000) * 8; // -10 ... +10
                int y = 4 - ((float)arc4random() / 0x100000000) * 8; // -10 ... +10
                midpoint.x += x;
                midpoint.y += y;
                [cardView setCenter:midpoint];
            }];
            
            if (![cardView.gestureRecognizers containsObject:cardView.panGestureRecognizer]) {
                [cardView.panGestureRecognizer addTarget:self action:@selector(pan:)];
                [cardView addGestureRecognizer:cardView.panGestureRecognizer];
            }

            if (![cardView.gestureRecognizers containsObject:cardView.tapGestureRecognizer]) {
                [cardView.tapGestureRecognizer addTarget:self action:@selector(tap:)];
                [cardView addGestureRecognizer:cardView.tapGestureRecognizer];
            }
        }
        
    }
}


#pragma mark - Abstract methods
-(CardMatchingGame *)createGameUsingDeck:(Deck *)deck { // abstract
    return nil;
}
-(Deck *)createDeck { // abstract
    return nil;
}
-(UIView *)createViewForCard:(Card *)card { // abstract
    return nil;
}
-(void)handleMatchedCard:(CardView *)cardView { // Abstract
    [NSException raise:@"Calling abstract method" format:@"Calling abstract method %s", __PRETTY_FUNCTION__];
}

@end
