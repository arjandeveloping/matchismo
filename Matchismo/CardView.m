//
//  CardView.m
//  Matchismo
//
//  Created by Arjan on 08/10/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//

#import "CardView.h"

@implementation CardView

-(void)handleMatchedCard {
    // Abstract
    [NSException raise:@"Calling abstract method \"handleMatchedCard\"" format:@"[CardView handleMatchedCard] is abstract."];
}

#pragma mark - Properties
//-(UIAttachmentBehavior *)attachmentBehavior {
//    if (!_attachmentBehavior) {
//        _attachmentBehavior = [[UIAttachmentBehavior alloc] init];
//    }
//    return _attachmentBehavior;
//}
-(UITapGestureRecognizer *)tapGestureRecognizer {
    if (!_tapGestureRecognizer) {
        _tapGestureRecognizer = [[UITapGestureRecognizer alloc] init];
    }
    return _tapGestureRecognizer;
}

-(UIPanGestureRecognizer *)panGestureRecognizer {
    if (!_panGestureRecognizer) {
        _panGestureRecognizer = [[UIPanGestureRecognizer alloc] init];
    }
    return _panGestureRecognizer;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
