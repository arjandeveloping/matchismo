//
//  CardGameHistoryViewController.h
//  Matchismo
//
//  Created by Arjan on 05/08/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardGameHistoryViewController : UIViewController
@property (strong, nonatomic) NSAttributedString *historyText;
@property (strong, nonatomic) NSArray *msgHistory; // of NSAttributedString
@end
