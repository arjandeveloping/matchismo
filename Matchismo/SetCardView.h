//
//  SetCard.h
//  Matchismo
//
//  Created by Arjan on 08/08/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CardView.h"

@interface SetCardView : CardView
@property (nonatomic) NSUInteger size; // 1 2 3
@property (nonatomic, strong) UIColor* color;
@property (nonatomic) NSUInteger shading; // 0 1 2
@property (nonatomic) NSUInteger symbol; // 0 1 2
@property (nonatomic) BOOL played;
@end