//
//  PlayingCardView.m
//  SuperCard
//
//  Created by Arjan on 07/08/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//
/* Special traits:
 - Not a subclass of UIViewController, but UIView
 - Generic playing card displaying view, not tied to the Matchismo
 model, totaly standalone
 - Remember, things that go in the view camp wanna be as generic and
 resuable as possible. So I can use this
 PlayingCardView in my pokergame app and also in Matchismo.
 - Always enforce this generic nature
 */

#import "PlayingCardView.h"

@interface PlayingCardView ()
@property (nonatomic) CGFloat faceCardScaleFactor;
@end


@implementation PlayingCardView

#pragma mark - Properties
@synthesize faceCardScaleFactor = _faceCardScaleFactor;
#define DEFAULT_FACE_CARD_SCALE_FACTOR 0.90

-(CGFloat)faceCardScaleFactor {
    if (!_faceCardScaleFactor) {
        _faceCardScaleFactor = DEFAULT_FACE_CARD_SCALE_FACTOR;
    }
    return _faceCardScaleFactor;
}

-(void)setFaceCardScaleFactor:(CGFloat)faceCardScaleFactor {
    _faceCardScaleFactor = faceCardScaleFactor;
    [self setNeedsDisplay];
}


-(void)setSuit:(NSString *)suit {
    _suit = suit;
    [self setNeedsDisplay];
    /* marks the receiver's entire bounds
    rectangle as needing to be redrawn (= call drawRect:). It lets
     iOS know that your view's visual is out of date. Can also be 
     called as -(void)setNeedsDisplayInRect:(CGRect)aRect;
     to only call drawRect for a certain CGRect.
    */
}

-(void)setRank:(NSUInteger)rank {
    /* For all the setters, we're calling setNeedsDisplay, for if
     someone changes the suit, or the rank, or faceup-ness */
    _rank = rank;
    [self setNeedsDisplay];
}

-(void)setFaceUp:(BOOL)faceUp {
    if (_faceUp != faceUp) {
        [UIView transitionWithView:self
                          duration:0.5
                           options:UIViewAnimationOptionTransitionFlipFromLeft
                        animations:^{
                            _faceUp = faceUp;
                        } completion:nil];
    } else {
        _faceUp = faceUp;
    }
    [self setNeedsDisplay];
}

-(void)pinch:(UIPinchGestureRecognizer *)gesture {
    if ((gesture.state == UIGestureRecognizerStateChanged) ||
        (gesture.state == UIGestureRecognizerStateChanged)) {
        // Then, i'm going to the faceCardScaleFactor and multiply
        // it by the scale of the gesture
        self.faceCardScaleFactor *= gesture.scale;
        // But, I don't want that to accumulate, so I'll set te gesture's
        // scale to 1.0 all the time, so that the next time I'll get
        // called, I'll get the incremental scale
        gesture.scale = 1.0;
    }
}

//- (IBAction)swipe:(UISwipeGestureRecognizer *)sender {
//    //if (!self.faceUp) {
//    //    [self drawRandomPlayingCard];
//    //}
//    self.faceUp = !self.faceUp;
//}

#pragma mark - Initialization

-(void)setup {
    self.backgroundColor = nil;
    self.opaque = NO; // no opaque + backgroundColor nil = yes transparent
    
    self.contentMode = UIViewContentModeRedraw; // If my bounds ever change, I want to get my drawRect: called
    // I also wanna do this in awakeFromNib because in this demo I am gonna create this view in a storyboard
    /* by default, when your UIView's bounds change, there is no redraw.
     Instead, the "bits" of your view will be stretched or squished or moved.
     Often, this is not what you want. Luckily, there's a UIView @property to
     control this: @property (nonatomic) UIViewContentMode contentMode;
     
     These content modes MOVE the bits of your drawing to that location
     UIViewContentMode{Left,Right,Top,Right,BottomLeft,BottomRight,TopLeft,TopRight}
     These content modes STRETCH the bits of your drawing
     UIViewContentModeScale{ToFill,AspectFill,AspectFit} // bit stretching / shrinking
     This content mode calls drawRect: to redraw everything when the bounds changes
     UIViewContentModeRedraw // It is quite often this is what you want.
     
     Default is UIViewContentModeScaleToFill (stretch the bits to fill the bounds)
     */
}

-(void)awakeFromNib { // Not doing alloc init of this view, so I need to setup stuff here
    [self setup];
}

- (id)initWithFrame:(CGRect)frame // designated initializer
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setup];
    }
    return self;
}

#pragma mark - Drawing

#define CORNER_FONT_STANDARD_HEIGHT 180.0
#define CORNER_RADIUS 12.0

-(CGFloat)cornerScaleFactor { return self.bounds.size.height / CORNER_FONT_STANDARD_HEIGHT; }
-(CGFloat)cornerRadius { return CORNER_RADIUS * [self cornerScaleFactor]; }
-(CGFloat)cornerOffset { return [self cornerRadius] / 3.0; }
/* cornerRadius in the bezierPath is how many points is in the radius as it goes around the 
 corner of the roundedRect. This number really depends on how big my card is. If I have a big
 card, I want a big radius, and with a small radius. So we created a cornerScaleFactor which
 is standardized to some height. And picked a radius that, at the CORNER_FONT_STANDARD_HEIGHT
 is 12, and that works pretty nice.
 */

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    UIBezierPath *roundedRect = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:[self cornerRadius]];
    /* self.bounds is my coordinate system, i.e. as big as possible */
    
    // First we're going to clip to the roundedRect, don't wanna draw outside the roundedRect
    [roundedRect addClip];
    
    [[UIColor whiteColor] setFill]; // sets the color of subsequent fill operations
    UIRectFill(self.bounds); // fills the rectangle (self.bounds) that is given as argument
    [[UIColor blackColor] setStroke];
    [roundedRect stroke];
    
    if (self.faceUp) {
        NSString *imageString = [NSString stringWithFormat:@"%@%@", [self rankAsString], self.suit];
        UIImage *faceImage = [UIImage imageNamed:imageString];
        if (faceImage) {
            // wanna move it in from the edges a little bit, dont want it to smash my corners
            // create rectangle to scale the image
            CGRect imageRect = CGRectInset(self.bounds, self.bounds.size.width * (1.0 - self.faceCardScaleFactor),
                                           self.bounds.size.height * (1.0 - self.faceCardScaleFactor));
            [faceImage drawInRect:imageRect];
        } else {
            [self drawPips];
        }
        //[[UIColor whiteColor] setFill]; // sets the color of subsequent fill operations
        //UIRectFill(self.bounds);
        [self drawCorners];
    } else {
        [[UIImage imageNamed:@"cardback"] drawInRect:self.bounds];
    }
}

- (NSString *)rankAsString {
    NSArray *ranks = @[@"?",@"A",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"J",@"Q",@"K"];
    NSString *rankString = nil;
    if (self.rank < [ranks count]) {
        rankString = ranks[self.rank];
    }
    return rankString;
}

-(void)drawCorners {
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.alignment = NSTextAlignmentCenter; // we'll set this as an attribute on the attr string
    
    UIFont *cornerFont = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    // We'll scale the font size, depending on the size of the card
    cornerFont = [cornerFont fontWithSize:cornerFont.pointSize * [self cornerScaleFactor]];
    
    NSAttributedString *cornerText = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@",
                                                                                 [self rankAsString], self.suit]
                                                                     attributes:@{NSFontAttributeName: cornerFont, NSParagraphStyleAttributeName: paragraphStyle} ];
    CGRect textBounds;
    textBounds.origin = CGPointMake([self cornerOffset], [self cornerOffset]);
    textBounds.size = [cornerText size]; // depends on string length
    
    [cornerText drawInRect:textBounds];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, self.bounds.size.width, self.bounds.size.height); // translate means move
    CGContextRotateCTM(context, M_PI); //M_PI = halfway around the circle
    [cornerText drawInRect:textBounds];
    
}

#pragma mark - Pips

#define PIP_HOFFSET_PERCENTAGE 0.165
#define PIP_VOFFSET1_PERCENTAGE 0.090
#define PIP_VOFFSET2_PERCENTAGE 0.175
#define PIP_VOFFSET3_PERCENTAGE 0.270

- (void)drawPips
{
    if ((self.rank == 1) || (self.rank == 5) || (self.rank == 9) || (self.rank == 3)) {
        [self drawPipsWithHorizontalOffset:0
                            verticalOffset:0
                        mirroredVertically:NO];
    }
    if ((self.rank == 6) || (self.rank == 7) || (self.rank == 8)) {
        [self drawPipsWithHorizontalOffset:PIP_HOFFSET_PERCENTAGE
                            verticalOffset:0
                        mirroredVertically:NO];
    }
    if ((self.rank == 2) || (self.rank == 3) || (self.rank == 7) || (self.rank == 8) || (self.rank == 10)) {
        [self drawPipsWithHorizontalOffset:0
                            verticalOffset:PIP_VOFFSET2_PERCENTAGE
                        mirroredVertically:(self.rank != 7)];
    }
    if ((self.rank == 4) || (self.rank == 5) || (self.rank == 6) || (self.rank == 7) || (self.rank == 8) || (self.rank == 9) || (self.rank == 10)) {
        [self drawPipsWithHorizontalOffset:PIP_HOFFSET_PERCENTAGE
                            verticalOffset:PIP_VOFFSET3_PERCENTAGE
                        mirroredVertically:YES];
    }
    if ((self.rank == 9) || (self.rank == 10)) {
        [self drawPipsWithHorizontalOffset:PIP_HOFFSET_PERCENTAGE
                            verticalOffset:PIP_VOFFSET1_PERCENTAGE
                        mirroredVertically:YES];
    }
}

#define PIP_FONT_SCALE_FACTOR 0.012

- (void)drawPipsWithHorizontalOffset:(CGFloat)hoffset
                      verticalOffset:(CGFloat)voffset
                          upsideDown:(BOOL)upsideDown
{
    if (upsideDown) [self pushContextAndRotateUpsideDown];
    CGPoint middle = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2);
    UIFont *pipFont = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    pipFont = [pipFont fontWithSize:[pipFont pointSize] * self.bounds.size.width * PIP_FONT_SCALE_FACTOR];
    NSAttributedString *attributedSuit = [[NSAttributedString alloc] initWithString:self.suit attributes:@{ NSFontAttributeName : pipFont }];
    CGSize pipSize = [attributedSuit size];
    CGPoint pipOrigin = CGPointMake(
                                    middle.x-pipSize.width/2.0-hoffset*self.bounds.size.width,
                                    middle.y-pipSize.height/2.0-voffset*self.bounds.size.height
                                    );
    [attributedSuit drawAtPoint:pipOrigin];
    if (hoffset) {
        pipOrigin.x += hoffset*2.0*self.bounds.size.width;
        [attributedSuit drawAtPoint:pipOrigin];
    }
    if (upsideDown) [self popContext];
}

- (void)drawPipsWithHorizontalOffset:(CGFloat)hoffset
                      verticalOffset:(CGFloat)voffset
                  mirroredVertically:(BOOL)mirroredVertically
{
    [self drawPipsWithHorizontalOffset:hoffset
                        verticalOffset:voffset
                            upsideDown:NO];
    if (mirroredVertically) {
        [self drawPipsWithHorizontalOffset:hoffset
                            verticalOffset:voffset
                                upsideDown:YES];
    }
}

- (void)pushContextAndRotateUpsideDown
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    CGContextTranslateCTM(context, self.bounds.size.width, self.bounds.size.height);
    CGContextRotateCTM(context, M_PI);
}

- (void)popContext
{
    CGContextRestoreGState(UIGraphicsGetCurrentContext());
}

@end
