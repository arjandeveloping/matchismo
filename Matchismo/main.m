//
//  main.m
//  Matchismo
//
//  Created by Arjan on 18/07/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CardGameAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CardGameAppDelegate class]));
    }
}
