//
//  CardsContainerView.m
//  Matchismo
//
//  Created by Arjan on 07/10/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//

#import "CardsContainerView.h"
#import "CardView.h"


@implementation CardsContainerView



-(void)resetGrid {
    [self.grid setSize:self.bounds.size];
    // Drawing code
    if ([self.grid inputsAreValid]) {
        int index = 0;
        for (int rowNo = 0; rowNo < self.grid.rowCount; rowNo++) {
            for (int columnNo = 0; columnNo < self.grid.columnCount; columnNo++) {
                if (index < [self.subviews count]) {
                    if ([self.subviews[index] isKindOfClass:[CardView class]]) {
                        CardView *crdView = self.subviews[index];
                        CGRect currentFrame = crdView.frame;
                        CGRect futureFrame = [self.grid frameOfCellAtRow:rowNo inColumn:columnNo];
                        if (!CGRectEqualToRect(currentFrame, futureFrame)) {
                            [UIView animateWithDuration:0.5 animations:^{
                                [self.subviews[index] setFrame:futureFrame];
                            }];
                        }
                    }
                    // if card is matched
                    // dont show
                }
                index++;
            }
        }
    }
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    [self resetGrid];
}

@end
