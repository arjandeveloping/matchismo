//
//  SetCardGameViewController.m
//  Matchismo
//
//  Created by Arjan on 31/07/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//

#import "SetCardGameViewController.h"
#import "SetDeck.h"
#import "SetCardMatchingGame.h"
#import "SetCard.h"
#import "SetCardView.h"

@interface SetCardGameViewController ()
@property (weak, nonatomic) IBOutlet UIButton *deal3MoreButton;
@end

@implementation SetCardGameViewController

#pragma mark - Initialization
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.deal3MoreButton setTitle:@"No more cards" forState:UIControlStateDisabled];
    [self.deal3MoreButton setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateUI];
}


#pragma mark - Properties

-(SetCardMatchingGame *)createGameUsingDeck:(Deck *)deck {
    return [[SetCardMatchingGame alloc] initWithCardCount:self.numberOfStartingCards usingDeck:[self createDeck] numberOfPlayers:1];
}
-(Deck *)createDeck {
    self.gameType = @"Set";
    return [[SetDeck alloc] init];
}

-(NSUInteger)numberOfStartingCards {
    return 12;
}

-(NSArray *)colors {
    return @[[UIColor greenColor],
             [UIColor redColor],
             [UIColor purpleColor]];
}


#pragma mark - Methods
-(void)resetGame {
    [super resetGame];
    self.deal3MoreButton.enabled = YES;
}

-(CardView *)createViewForCard:(Card *)card {
    SetCardView *cardView = [[SetCardView alloc] init];
    SetCard *setCard = (SetCard *)card;
    cardView.color = [self colors][setCard.color];
    cardView.symbol = setCard.symbol;
    cardView.size = [[SetCard validSizes][setCard.size] integerValue];
    cardView.shading = setCard.shading;
    [cardView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapSetCard:)]];
    return cardView;
}

- (IBAction)deal3More:(UIButton*)sender {
    if (![self addCards:3]) {
        if ([[self.game matchingCombinations] count]) {
            // penalize user for adding cards while there is still a set
        }
        sender.enabled = false;
    }
}

-(void)tapSetCard:(UITapGestureRecognizer *)sender {
    if ([sender.view isKindOfClass:[SetCardView class]]) {
        //PlayingCardView *view = (PlayingCardView *)sender.view;
        //view.faceUp = !view.faceUp;
        //NSUInteger chosenButtonIndex = [self.cardButtons indexOfObject:sender];
        [self.game chooseCardAtIndex:[self.cardViews indexOfObject:sender.view]];
        [self updateUI];
    }
}

-(void)updateUI {
    [super updateUI];
    for (SetCardView *view in self.cardViews) {
        SetCard *card = (SetCard *)[self.game cardAtIndex:[self.cardViews indexOfObject:view]];
        if (card.matched) {
            view.layer.borderColor = [UIColor greenColor].CGColor;
            view.layer.borderWidth = 3.0f;
        }
        else if (card.chosen) {
            view.layer.borderColor = [UIColor blueColor].CGColor;
            view.layer.borderWidth = 3.0f;
        } else {
            view.layer.borderWidth = 0.0f;
        }
    }
}
- (IBAction)touchHintsButton {
    [self updateUI];
    NSArray *validSets = [self.game matchingCombinations];
    if ([validSets count] > 0) {
        id obj = validSets[0];
        if ([obj isKindOfClass:[NSArray class]]) {
            NSArray *arrayOfCardIds = (NSArray *)obj;
            for (id obj2 in arrayOfCardIds) {
                if ([obj2 isKindOfClass:[NSNumber class]]) {
                    NSNumber *number = (NSNumber *)obj2;
                    CardView *cardView = self.cardViews[[number integerValue]];
                    if (cardView.window) { // if onscreen
                        cardView.layer.borderColor = [UIColor darkGrayColor].CGColor;
                        cardView.layer.borderWidth = 3.0f;
                    }
                }
            }
        }
    } else {
        [self checkForValidCombinations];
    }
}

-(void)handleMatchedCard:(CardView *)cardView {
    //
}

@end
