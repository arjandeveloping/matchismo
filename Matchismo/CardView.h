//
//  CardView.h
//  Matchismo
//
//  Created by Arjan on 08/10/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardView : UIView
-(void)handleMatchedCard; // Abstract
@property (strong, nonatomic) UIAttachmentBehavior *attachmentBehavior;
@property (strong, nonatomic) UIPanGestureRecognizer *panGestureRecognizer;
@property (strong, nonatomic) UITapGestureRecognizer *tapGestureRecognizer;
@end
