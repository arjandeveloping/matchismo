//
//  CardGameSettingsViewController.m
//  Matchismo
//
//  Created by Arjan on 06/08/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//

#import "CardGameSettingsViewController.h"
#import "GameSettings.h"

@interface CardGameSettingsViewController ()
@property (weak, nonatomic) IBOutlet UILabel *matchBonusLabel;
@property (weak, nonatomic) IBOutlet UILabel *mismatchPenaltyLabel;
@property (weak, nonatomic) IBOutlet UILabel *costToChooseLabel;

@property (weak, nonatomic) IBOutlet UIStepper *matchBonusStepper;
@property (weak, nonatomic) IBOutlet UIStepper *mismatchPenaltyStepper;
@property (weak, nonatomic) IBOutlet UIStepper *costToChooseStepper;
@end

@implementation CardGameSettingsViewController
- (IBAction)matchBonusStepperValueChanged:(UIStepper *)sender {
    [GameSettings setMatchBonus:self.matchBonusStepper.value];
    [self updateUI];
}
- (IBAction)mismatchPenaltyStepperValueChanged:(UIStepper *)sender {
    [GameSettings setMismatchPenalty:self.mismatchPenaltyStepper.value];
    [self updateUI];
}
- (IBAction)costToChooseStepperValueChanged:(UIStepper *)sender {
    [GameSettings setCostToChoose:self.costToChooseStepper.value];
    [self updateUI];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self updateUI];
}

-(void)updateUI {
    self.matchBonusLabel.text = FORMAT(@"%lu", (unsigned long)[GameSettings matchBonus]);
    self.mismatchPenaltyLabel.text = FORMAT(@"%lu", (unsigned long)[GameSettings mismatchPenalty]);
    self.costToChooseLabel.text = FORMAT(@"%lu", (unsigned long)[GameSettings costToChoose]);
}

@end
