//
//  TwoPlayerSetCardGameViewController.m
//  Matchismo
//
//  Created by Arjan on 22/12/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//

#import "TwoPlayerSetCardGameViewController.h"
#import "SetCardMatchingGame.h"

@interface TwoPlayerSetCardGameViewController ()
@property (weak, nonatomic) IBOutlet UILabel *playerNumberLabel;
@end

@implementation TwoPlayerSetCardGameViewController

-(SetCardMatchingGame *)createGameUsingDeck:(Deck *)deck {
    return [[SetCardMatchingGame alloc] initWithCardCount:self.numberOfStartingCards usingDeck:[self createDeck] numberOfPlayers:2];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)switchPlayer {
    [self.game switchPlayer];
    [self updateUI];
}

-(void)updateUI {
    [super updateUI];
    [self.playerNumberLabel setText:FORMAT(@"Player %lu", (self.game.currentPlayer+1))];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
