//
//  PlayingCardGameViewController.m
//  Matchismo
//
//  Created by Arjan on 30/07/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//

#import "PlayingCardGameViewController.h"
#import "PlayingCardDeck.h"
#import "PlayingCardMatchingGame.h"
#import "PlayingCardView.h"
#import "PlayingCard.h"
#import "Grid.h"

@interface PlayingCardGameViewController ()

@end

@implementation PlayingCardGameViewController

#pragma mark - Properties

-(Deck *)createDeck {
    self.gameType = @"Playing";
    return [[PlayingCardDeck alloc] init];
}
-(PlayingCardMatchingGame *)createGameUsingDeck:(Deck *)deck {
    return [[PlayingCardMatchingGame alloc] initWithCardCount:self.numberOfStartingCards usingDeck:deck numberOfPlayers:1];
}
-(NSUInteger)cardCount {
    return self.numberOfStartingCards;
}
-(NSUInteger)numberOfStartingCards {
    NSInteger amount = [PlayingCard validSuits].count * [PlayingCard maxRank];
    return amount <= 15 ? amount : 15;
    //return 4;
}


#pragma mark - Methods

-(CardView *)createViewForCard:(Card *)card{
    PlayingCardView *cardView = [[PlayingCardView alloc] init];
    PlayingCard *playingCard = (PlayingCard *)card;
    cardView.rank = playingCard.rank;
    cardView.suit = playingCard.suit;
    UITapGestureRecognizer *swipeRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                          action:@selector(swipe:)];
    //[swipeRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight | UISwipeGestureRecognizerDirectionLeft)];
    [cardView addGestureRecognizer:swipeRecognizer];
    return cardView;
}

-(void)swipe:(UITapGestureRecognizer *)sender {
        if (sender.state == UIGestureRecognizerStateRecognized) {
            if ([sender.view isKindOfClass:[PlayingCardView class]]) {
            CardView *view = (CardView *)sender.view;
            [self.game chooseCardAtIndex:[self.cardViews indexOfObject:view]];
            [self updateUI];
        }
    }
}

-(void)handleMatchedCard:(CardView *)cardView {
    if ([cardView isKindOfClass:[PlayingCardView class]]) {
        // eerst faceup
        PlayingCardView *playingCardView = (PlayingCardView *)cardView;
        playingCardView.faceUp = YES;
    }
}

-(void)updateUI {
    [super updateUI];
    for (PlayingCardView *view in self.cardViews) {
        PlayingCard *card = (PlayingCard *)[self.game cardAtIndex:[self.cardViews indexOfObject:view]];
        if (!card.matched) {
            view.faceUp = card.chosen ? YES : NO;
        }
    }
}

-(void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark - Other
// This is the designated initializer, but not sure why they put that here
// since nowadays we pull these things out of the storyboards, we would use
// awakeFromNib or even better viewDidLoad.
//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}
@end
