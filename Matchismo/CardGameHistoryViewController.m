//
//  CardGameHistoryViewController.m
//  Matchismo
//
//  Created by Arjan on 05/08/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//

#import "CardGameHistoryViewController.h"

@interface CardGameHistoryViewController ()
@property (weak, nonatomic) IBOutlet UITextView *historyTextView;
@end

@implementation CardGameHistoryViewController

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateUI];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)updateUI {
    if ([self.msgHistory count]) {
        NSMutableAttributedString *historyText = [[NSMutableAttributedString alloc] init];
        for (id object in self.msgHistory) {
            if ([object isKindOfClass:[NSAttributedString class]]) {
                NSAttributedString *msg = (NSAttributedString *)object;
                [historyText appendAttributedString:msg];
                [historyText appendAttributedString: [[NSAttributedString alloc] initWithString:@"\n"]];
            }
        }
        // catch and preserve the font of the UITextView
        UIFont *font = [self.historyTextView.textStorage attribute:NSFontAttributeName atIndex:0 effectiveRange:NULL]; // catch
        [self.historyTextView.textStorage setAttributedString: historyText];
        [self.historyTextView.textStorage addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, self.historyText.length)]; //re-set
    } else {
        [self.historyTextView setAttributedText: [[NSAttributedString alloc] initWithString:@"No history found"]];
    }
    //self.automaticallyAdjustsScrollViewInsets = NO; // cancel inset
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
