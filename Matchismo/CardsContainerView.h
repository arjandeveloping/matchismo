//
//  CardsContainerView.h
//  Matchismo
//
//  Created by Arjan on 07/10/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Grid.h"

@interface CardsContainerView : UIView
@property (nonatomic, strong) Grid *grid;
-(void)resetGrid;
@end
