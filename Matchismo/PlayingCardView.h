//
//  PlayingCardView.h
//  SuperCard
//
//  Created by Arjan on 07/08/14.
//  Copyright (c) 2014 Auxilium B.V. All rights reserved.
//
/* Things to note:
 Remember, keep it as generic as possible. We need to be able to
 specify which card we need to display, but using a Card* or 
 PlayingCard* as property would be a bad idea, since it will make 
 the class less generic.
 Instead, we just want to know the rank and the suit.
 */
#import <UIKit/UIKit.h>
#import "CardView.h"

@interface PlayingCardView : CardView

// Propetrties
@property (nonatomic) NSUInteger rank;
@property (nonatomic, strong) NSString *suit;
@property (nonatomic) BOOL faceUp;

// Methods
- (void)pinch:(UIPinchGestureRecognizer *)gesture; // so that my controller knows that my card view is capable of this

@end
